#!/usr/bin/env python
# Translate one word into several languages at once using google translate.

import pycurl
import sys
import os

def translateWord(word, source, target):
    with open("response.html", "w") as response:
        curl = pycurl.Curl()
        curl.setopt(curl.URL, "http://translate.google.com")
        curl.setopt(curl.WRITEDATA, response)
        curl.setopt(curl.POSTFIELDS, 
                "sl=%s&tl=%s&text=%s" % 
                (source, target, word))
        curl.perform()
        curl.close()

    # by johnraff @ http://crunchbang.org/forums/viewtopic.php?pid=177999
    os.system("iconv -f utf8 < response.html 2>iconv.err | \
               awk 'BEGIN {RS=\"</div>\"};/<span[^>]* id=[\"\'\\\'\']?result\
               _box[\"\'\\\'\']?/' | html2text -utf8")

if len(sys.argv) < 2:
    sys.exit("Usage: %s <word> [source_lang]" % sys.argv[0])

word = sys.argv[1]

# The default source language is polish
if len(sys.argv) > 2:
    source = sys.argv[2]
else:
    source = "pl"

languages = ["en","af","sq","az","eu","be","b","ca","hr","cs","da","nl","et","tl","fi","fr","l","de","u","ht","hu","is","id","a","it","la","lv","lt","ms","mt","no","pl","pt","ro","ru","sr","sk","sl","es","sw","sv","tr","cy"]

for lang in languages:
    translateWord(word, source, lang)

